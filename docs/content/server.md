+++
title = "Setup"
date = "2016-12-25"
sidemenu = "true"
description = "Setting up a FIND server"
thumbnail = "https://www.internalpositioning.com/find-example.png"
keywordlist = "server, golang, install, internal nagivation, indoor positioning, positioning"
+++


The main functioning of the server is to allow devices to send fingerprints to it and perform the machine learning classifications of the locations. The instructions below will assist in getting the FIND server started and running.

<center>
<img src="/find-example.png" class="pure-img"></img>
</center>


# TOC

- [Install with Go](#install-with-go)
  * [MQTT support](#mqtt-support)
  * [Random Forest support](#random-forest-support)
  * [SVM support](#svm-support)
  * [Filtering macs](#filtering-macs)
- [Install with Docker](#install-with-docker)


# Install with Go

First [install Go](https://golang.org/dl/) if you haven't already. FIND is tested on Go version 1.5+.

```
$ git clone https://github.com/schollz/find.git
$ cd find
$ go get ./...
$ go build
```

Then to run,
```
$ ./find
-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----
   _________  _____
  / __/  _/ |/ / _ \  ______ _____  _____ ____
 / _/_/ //    / // / (_-< -_) __/ |/ / -_) __/
/_/ /___/_/|_/____/ /___|__/_/  |___/\__/_/

(version 2.X) is up and running on http://192.168.1.2:8003
-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----

```

## MQTT support

### Setup (Self-hosted servers)

Basically, right now you can only run `mosquitto` using a instance running from a configuration file created specifically by *FIND*. To get started, you'll first need the latest copy of `mosquitto`:

```bash
wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
sudo apt-key add mosquitto-repo.gpg.key
cd /etc/apt/sources.list.d/
sudo wget http://repo.mosquitto.org/debian/mosquitto-wheezy.list
sudo apt-get update
sudo apt-get install mosquitto-clients mosquitto
```

Then goto your FIND folder and create a file (in the future I'll have FIND do this automatically):

```
mkdir /path/to/find/mosquitto
touch /path/to/find/mosquitto/conf
```

Now, start `mosquitto` in the background:

```
mosquitto -c /path/to/find/mosquitto/conf -d
```

Now, you can startup **FIND**:

```bash
./find -mqtt ADDRESS:1883 -mqttadmin ADMIN -mqttadminpass ADMIN_PASS -mosquitto `pgrep mosquitto` -p :PORT ADDRESS:PORT
```

The `ADDRESS` and `PORT` is the address and port your using for **FIND**. The `pgrep mosquitto` is for getting the `mosquitto` PID, which is used to send SIGHUP to reload the passwd file. The `ADMIN` and the `ADMIN_PASS` are your secret name and password to access read/write access to every MQTT channel. Make sure these are not simple enough to guess.

That's it!

### Client (MQTT connections)

#### Register
To receive data from the **FIND** MQTT, follow these steps. First, register your group using the following:

```bash
curl -X PUT "https://ml.internalpositioning.com/mqtt?group=YOURGROUP"
```

where `YOURGROUP` is your group name. This command will tell **FIND** to add group level access to your own special MQTT channel. You'll receive a message like:

```javascript
{
    "message": "You have successfully set your password.",
    "password": "YOURPASSWORD",
    "success": true
}
```

The `password` is what you can use to access MQTT now. You can retrieve your password by using the same `curl` command. These passwords are completely random, and not hashed - so totally not guessable.

#### Subscribing

First make sure to register. To subscribe to your channel to see current locations, simply use the topic `YOURGROUP/location/#`, e.g.:

```bash
mosquitto_sub -h ml.internalpositioning.com -u YOURGROUP -P YOURPASSWORD -t "YOURGROUP/location/#"
```

#### Publishing Fingerprints

Currently, MQTT takes only a very specific type of fingerprint. Basically, to utilize the minimal MQTT byte size you have to compress the mac addresses and RSSI components.

To publish fingerprints, use the channel `YOURGROUP/track/USERNAME` for tracking or `YOURGROUP/learn/USERNAME/LOCATION` for learning. The body needs to be a multiple of 14 bytes where the first 12 bytes are the Mac address and the next 2 bytes is the RSSI value (absolute value). For example, if your detected routers are

```javascript
"ab:cd:ef:gf:ij:kl":-32
"mn:op:qr:st:uv:wx":-3
```

then you'll need to send the following as the body:

```
"abcdefgfijkl32mnopqrstuvwx 3"
```

## Random Forest support

FIND can use random forests with an additional TCP server that handles Random Forest calculations. To use it, first install Python3 libraries:

```
apt-get install -y python3 python3-dev python3-pip
apt-get install -y python3-scipy python3-numpy
python3 -m pip install scikit-learn
```

Then run the Random Forests TCP server using

```
python3 rf.py --port 5009
```

Now you can run the FIND server using this server for the RF calculations with some new flags to tell the server which port is on:

```
./findserver -rf 5009
```

## SVM support


Follow these instructions if you are running a FIND server and would like to add SVM to the machine learning routines.

### Setup (Self-hosted servers)

FIND will automatically utilize `libsvm` once it is installed. Here are the instructions to install (you should run with root/sudo):

```
sudo apt-get install g++
wget http://www.csie.ntu.edu.tw/~cjlin/cgi-bin/libsvm.cgi?+http://www.csie.ntu.edu.tw/~cjlin/libsvm+tar.gz
tar -xvf libsvm-*.tar.gz
cd libsvm-*
make
cp svm-scale /usr/local/bin/
cp svm-predict /usr/local/bin/
cp svm-train /usr/local/bin/
```

Then just restart FIND! It will automatically detect whether its installed. When SVM is enabled, you will see SVM data along with the Naive-Bayes information.

_Note_: Currently FIND defaults to use the Naive-Bayes machine learning for the actual guesses. In my experience
SVM is generally inferior, but this may depend on your location.

## Filtering macs

There are some instances where you want to ignore most access points and use only a select few. You can now do this by starting the server with the `-filter` flag:

```
./findserver -filter macs.json
```

where the file, `macs.json` contains *only the macs you want to use*. For example, a `macs.json` could look like:

```javascript
{
    "ac:86:74:6b:9b:80":true,
    "ac:86:74:6b:9b:60":true,
    "ac:86:74:6b:9b:a0":true
}
```

which would only do calculations based on those three access points. 

__Note__: All access points are saved, even when `-filter` is enabled. However, calculations will only be performed using the ones specified in the filter file.

<br><hr><br>

# Install with Docker

This installation route will include `mosquitto` and `SVM` and Random Forests - no further configuration needed. Its based off the Ubuntu16 image, but that could be changed (except `mosquitto` is not bundled in earlier versions). This Dockerfile makes more sense to me since it reads more like a Makefile. It forwards port `18003` for FIND and port `11883` for `mosquitto` in the following examples.

## Using hub.docker.com

```
$ docker pull schollz/find
$ docker run -it -p 18003:8003 -p 11883:1883 -v /path/to/host/data/folder:/data schollz/find
```

## Using Dockerfile

```
$ git clone https://github.com/schollz/find.git && cd find
$ docker build -t finddocker .
$ docker run -it -p 18003:8003 -p 11883:1883 -v /path/to/host/data:/data finddocker
```

<br><hr><br>

