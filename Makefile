.PHONY: build
build:
	cd docs && hugo && cd ../
	cd splashsite && hugo && cd ../
	rm -rf public
	mv docs/public public
	rsync -avrP --exclude 'sitemap.xml' splashsite/public/ public/
	rm -rf splashsite/public
	cd public
	python3 -m http.server

.PHONY: release
release:
	cd docs && hugo && cd ../
	cd splashsite && hugo && cd ../
	rm -rf public
	mv docs/public public
	rsync -avrP --exclude 'sitemap.xml' splashsite/public/ public/
	rm -rf splashsite/public
	rsync -avrP public/ zns@cowyo.com:/www/www.internalpositioning.com/
