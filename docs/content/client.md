+++
title = "Clients"
date = "2016-12-25"
sidemenu = "true"
description = "Clients that you can use with FIND"
thumbnail = "https://www.internalpositioning.com/android-boot-logo.jpg"
keywordlist = "internal positioning, indoor positioning, particle, photon, android, electric imp, esp8266"
+++

FIND supports many different types of clients. The code to use the FIND server is very simple, so it is generalizable to many devices. Click on a device below to get started on a particular device.

<div class="pure-g">
    <div class="pure-u-1-5">
        <a href="#android">
        <img class="pure-img-responsive" src="/android-boot-logo.jpg" alt="Android App">
        </a>
    </div>
    <div class="pure-u-1-5">
        <a href="#electric-imp">
        <img class="pure-img-responsive" src="/electric-imp-logo.png" alt="Electric Imp">
        </a>
    </div>
    <div class="pure-u-1-5">
        <a href="#raspberry-pi">
        <img class="pure-img-responsive" src="/raspberry-pi-logo.png" alt="Raspberry Pi">
        </a>
    </div>
    <div class="pure-u-1-5">
        <a href="#particle-photon">
        <img class="pure-img-responsive" src="/particle-logo.png" alt="Particle Photon">
        </a>
    </div>
    <div class="pure-u-1-5">
        <a href="#esp8266">
        <img class="pure-img-responsive" src="/esp8266.png" alt="Particle Photon">
        </a>
    </div>
</div>


<br><br>

# Android

<div class="pure-g">
<div class="pure-u-1-3"></div>
<div class="pure-u-1-3"><img class="pure-img-responsive" src="/android-boot-logo.jpg" alt="Android App">
</div>
<div class="pure-u-1-3"></div>
</div>

The current app can be downloaded from the Google Play store for free. The current version is available [here](https://play.google.com/store/apps/details?id=com.hcp.find&hl=en).

Alternatively, there is a 3rd party app that has optional Tasker integration available [here on the Google Play store](https://play.google.com/store/apps/details?id=com.seversion.roomplus&hl=en).

The source-code for the current app is available [on Github](https://github.com/schollz/find-client-android). The source-code for the old app is also available [on Github](https://github.com/schollz/find/tree/android).

# Electric Imp

<div class="pure-g">
<div class="pure-u-1-3"></div>
<div class="pure-u-1-3"><img class="pure-img-responsive" src="/electric-imp-logo.png" alt="Android App">
</div>
<div class="pure-u-1-3"></div>
</div>

The Electric Imp is an amazingly easy way to integrate just about any hardware device to the internet. The [impExplorer](https://electricimp.com/docs/hardware/resources/reference-designs/explorerkit/) is a real ideal client for FIND as it can include a wake-on-shake with its builtin accelerometer. Its also inexpensive and easy to program.

The program I've used is below.

<script src="https://gist.github.com/schollz/b815f746fca378b1408f17acea269edd.js"></script>

# Raspberry Pi

<div class="pure-g">
<div class="pure-u-1-3"></div>
<div class="pure-u-1-3"><img class="pure-img-responsive" src="/raspberry-pi-logo.png" alt="Android App">
</div>
<div class="pure-u-1-3"></div>
</div>

Raspberry Pi is very easy to use as a client for FIND.

To get started, first [download the findclient for ARM](https://github.com/schollz/find/releases/download/v0.5/findclient_0.5_linux_arm.zip). Then unzip and just run!

Here are the commands you can use to get started:

```
wget https://github.com/schollz/find/releases/download/v0.5/findclient_0.5_linux_arm.zip
unzip findclient_0.5_linux_arm.zip
sudo ./findclient -g YOURGROUP         (for tracking)
sudo ./findclient -g YOURGROUP -e      (for learning)
```



# Particle Photon

<div class="pure-g">
<div class="pure-u-1-3"></div>
<div class="pure-u-1-3"><img class="pure-img-responsive" src="/particle-logo.png" alt="Android App">
</div>
<div class="pure-u-1-3"></div>
</div>

The Particle Photon is a $20 device that can be used as a tracking device using the following code. The code allows for a "sleep" mode that can be activated by pressing "Setup" once, and turned off by pressing "Setup" twice. This mode allows more battery life.

You can access the **FIND** program using HTTP or MQTT. I've included example sketches that can be used for each.


### HTTP-based messaging

<script src="https://gist.github.com/schollz/6077b4c64cf488e89856ed76c0f8a7d2.js"></script>

Some notes:

- You must use HTTP, not HTTPS. That's why the server is set to `ml2.internalpositioning.com`
- You can not flash from WiFi is the board is in sleep mode. Thats what the button is for. If this fails, you can reset by unplugging, holding down "Setup" and then plugging in while holding down "Setup." Then link up the Photon like you did from the beginning.
- The Photon ESP chip sees fewer macs than a Android does, probably because of the antenna. Thus, its best to not use platform-specific information and you should set the mixins to `0` on the server (see API).


### MQTT-based messaging

This version uses significantly less bandwidth than the HTTP version. It takes a little more configuration, [see the MQTT documentation](/mqtt/) for how to get started with MQTT.

<script src="https://gist.github.com/schollz/d388b0c0ed1bb3b604eba6b7154a49d1.js"></script>


**Benchmarking**

Using a `nextTime` of `+2000ms` it uses 147 mA.

Using a `nextTime` of `+5000ms`, it uses 113 mA.

Using a `nextTime` of `+10000ms`, with SLEEP ACTIVATED, it uses 81 mA

Using a `nextTime` of `+60000ms`, with SLEEP ACTIVATED, it uses 57 mA

# ESP8266

<div class="pure-g">
<div class="pure-u-1-3"></div>
<div class="pure-u-1-3"><img class="pure-img-responsive" src="/esp8266.png" alt="Android App">
</div>
<div class="pure-u-1-3"></div>
</div>

Coming soon! You can track the progress or contribute [at the Github Issue](https://github.com/schollz/find/issues/118).