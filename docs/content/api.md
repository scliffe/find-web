+++
title = "API"
date = "2016-12-25"
sidemenu = "true"
description = "API for using the FIND server"
thumbnail = "https://www.internalpositioning.com/rest.png"
keywordlist = "server, golang, install, internal nagivation, indoor positioning, positioning"
+++


<div class="pure-g">
        <div class="pure-u-19-24">
The API for getting and setting fingerprints, and manipulating variables in the database can be done using simple RESTful web services.
<ul>
<li><a href="#post-learn">POST /learn</a></li>
<li><a href="#post-track">POST /track</a></li>
<li><a href="#get-calculate">GET /calculate</a></li>
<li><a href="#get-location">GET /location</a></li>
<li><a href="#delete-username">DELETE /username</a></li>
<li><a href="#delete-locations">DELETE /locations</a></li>
<li><a href="#delete-database">DELETE /database</a></li>
<li><a href="#put-mixin">PUT /mixin</a></li>
<li><a href="#put-database">PUT /database</a></li>
<li><a href="#put-mqtt">PUT /mqtt</a></li>
<li><a href="#get-status">GET /status</a></li>
<li><a href="#get-group-dashboard">GET /GROUP/dashboard</a></li>
</ul>
        </div>
        <div class="pure-u-1-24"></div>
        <div class="pure-u-4-24"><img class="pure-img-responsive" src="/rest.png"></div>
</div>


## [POST /learn](#post--learn)

### Description

Submit a fingerprint to be used for learning the classification of the location. The information for the fingerprint is gathered from the WiFi client - either the App or the program.

### Parameters

#### POST

```json
{
   "group":"some group",
   "username":"some user",
   "location":"some place",
   "time":12309123,
   "wifi-fingerprint":[
      {
         "mac":"AA:AA:AA:AA:AA:AA",
         "rssi":-45
      },
      {
         "mac":"BB:BB:BB:BB:BB:BB",
         "rssi":-55
      }
   ]
}
```

### Response

```json
{
    "success": true,
    "message": "Inserted fingerprint containing 23 APs for zack at zakhome floor 2 office"
}
```

<br><hr><br>

## POST /track

Submit a fingerprint to be used for classifying the location. The information for the fingerprint is gathered from the WiFi client - either the App or the program.

### Parameters

#### POST

```json
{
   "group":"some group",
   "username":"some user",
   "location":"some place",
   "time":12309123,
   "wifi-fingerprint":[
      {
         "mac":"AA:AA:AA:AA:AA:AA",
         "rssi":-45
      },
      {
         "mac":"BB:BB:BB:BB:BB:BB",
         "rssi":-55
     }
   ]
}
```

### Response

```json
{
    "success": true,
    "message": "Calculated location: zakhome floor 2 office",
    "location": "zakhome floor 2 office",
    "bayes": {
        "zakhome floor 1 kitchen": 0.07353831034486494,
        "zakhome floor 2 bedroom": -0.9283974092154644,
        "zakhome floor 2 office": 0.8548590988705993
    }
}
```

<br><hr><br>

## GET /calculate

### Description

Recalculates the priors for the database for the `group`.

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>group</td>
<td>query</td>
<td>Defines the unique group ID</td>
<td>yes</td>
</tr>
</tbody>
</table class="pure-table">

### Response

```json
{
  "message":"Parameters optimized",
  "success":true
}
```

<br><hr><br>

## GET /location

### Description

Gets the locations for the specified user(s) in the specified group.

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>group</td>
<td>query</td>
<td>Defines the unique group ID</td>
<td>yes</td>
</tr>
<tr class="even">
<td>user</td>
<td>query</td>
<td>Specifies a user to get location</td>
<td>no</td>
</tr>
<tr class="odd">
<td>users</td>
<td>query</td>
<td>Specifies multiple users <code>users=X,Y,Z</code> to get histories</td>
<td>no</td>
</tr>
<tr class="even">
<td>n</td>
<td>query</td>
<td>Specifies number of fingerprints to get from history</td>
<td>no</td>
</tr>
</tbody>
</table class="pure-table">

### Response

If `user` or `users` are not specified, then the location of all users are returned.

```json
{
   "message":"Correctly found locations.",
   "success":true,
   "users":{
      "morpheus":[
         {
            "time":"2016-04-18 15:59:38.146929368 -0400 EDT",
            "location":"office",
            "bayes":{
               "bed bath":-1.0796283868148098,
               "bedroom":-0.3253323338565688,
               "car":-0.11084494825121938,
               "dining":-0.21592336935362944,
               "kitchen":0.7779455402822841,
               "living":-0.5328733505357962,
               "office":1.486656848529739
            }
         }
      ],
      "zack":[
         {
            "time":"2016-04-20 07:27:47.960140659 -0400 EDT",
            "location":"office",
            "bayes":{
               "bed bath":-1.028454724759723,
               "bedroom":0.1239023145100694,
               "car":-0.1493711750580678,
               "dining":-0.4237049232002753,
               "kitchen":0.6637176338607336,
               "living":-0.701636080467658,
               "office":1.515546955114921
            }
         }
      ]
   }
}
```

<br><hr><br>

## DELETE /username

### Description

Deletes all the tracking fingerprints for specified user in the specified group.

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>group</td>
<td>query</td>
<td>Defines the unique group ID</td>
<td>yes</td>
</tr>
<tr class="even">
<td>user</td>
<td>query</td>
<td>Specifies a user</td>
<td>yes</td>
</tr>
</tbody>
</table class="pure-table">

### Response

```json
{
  "success":true,
  "message":"Deleted user Y"
}
```

<br><hr><br>

## DELETE /locations

### Description

Bulk delete locations

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>group</td>
<td>query</td>
<td>Defines the unique group ID</td>
<td>yes</td>
</tr>
<tr class="even">
<td>names</td>
<td>query</td>
<td>Enter locations seperated by commas, e.g. locations=X,Y,Z</td>
<td>yes</td>
</tr>
</tbody>
</table class="pure-table">

### Response

```json
{
  "success":true,
  "message":"Deleted X locations"
}
```

<br><hr><br>

## DELETE /database

### Description

Delete database and all associated data.

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>group</td>
<td>query</td>
<td>Defines the unique group ID</td>
<td>yes</td>
</tr>
</tbody>
</table class="pure-table">

### Response

```json
{
  "success":true,
  "message":"Successfully deleted X."
}
```

<br><hr><br>

## PUT /mixin

### Description

Allows overriding of the `Mixin` parameter. Value of `0` uses only the RSSI Priors, while value of `1` uses only the Mac prevalence statistics.

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>group</td>
<td>query</td>
<td>Defines the unique group ID</td>
<td>yes</td>
</tr>
<tr class="even">
<td>mixin</td>
<td>query</td>
<td>Specifiy a value between 0 and 1 to activate, or -1 to deactivate</td>
<td>yes</td>
</tr>
</tbody>
</table class="pure-table">

### Response

```json
{
  "message":"Overriding mixin for testdb, now set to 1",
  "success":true
}
```

<br><hr><br>

## PUT /database

### Description

Migrate a database. This copies all the contents of one database to another. If the group does not exist, it will be created. The group that is migrated from is not deleted.

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>from</td>
<td>query</td>
<td>Defines the unique group to migrate from</td>
<td>yes</td>
</tr>
<tr class="even">
<td>to</td>
<td>query</td>
<td>Defines the unique group to migrate database into</td>
<td>yes</td>
</tr>
</tbody>
</table class="pure-table">
### Response

```json
{
  "message":"Successfully migrated X to Y",
  "success":true
}
```

<br><hr><br>

## PUT /mqtt

### Description

Allows you to access MQTT streams of your data. This is available on the public server using the 3rd party [mosquitto server](https://mosquitto.org/), if you want to setup, [see this documentation](https://doc.internalpositioning.com/mqtt/).

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>group</td>
<td>query</td>
<td>Defines the unique group ID</td>
<td>yes</td>
</tr>
</tbody>
</table class="pure-table">

### Response

```json
{
    "message": "You have successfully set your password.",
    "password": "YOURPASSWORD",
    "success": true
}
```

<br><hr><br>

## GET /status

### Description

Returns status of the server and some information about the computer.

### Parameters

None.

### Response

```json
{
  "num_cores":1,
  "registered":"2016-04-16 14:55:34.483803377 -0400 EDT",
  "status":"standard",
  "uptime":35109.225647597
}
```

<br><hr><br>

## GET /GROUP/dashboard

### Description

A dashboard website for the specified group. You can optionally pass a parameter that will filter the specified users for the live realtime tracking.

### Parameters

<table class="pure-table">
<thead>
<tr class="header">
<th>Name</th>
<th>Location</th>
<th>Description</th>
<th>Required</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>user</td>
<td>query</td>
<td>User to filter</td>
<td>no</td>
</tr>
<tr class="even">
<td>users</td>
<td>query</td>
<td>Users, seperated by comma, to filter</td>
<td>no</td>
</tr>
</tbody>
</table class="pure-table">

